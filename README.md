# ABOUT

##Mountain Man. 
##A theme for ghost CMS.

By Alejandro Andrés Leal


## Changelog

### Version 0.0.0 [Done]
- Initial commit.

### Vesion 0.0.1 [Done]
- Setup local Ghost instance
- Create theme basic structure, code name: mountain-man
- Configure admin login
- Outline development tasks
- Homepage design mockup
- Content page design mockup
- Update index.hbs file, add default.hbs file

### Version 0.0.2 [Done]
- Markup and styles for homepage designs
- Markup and styles for content page designs
- AWS Instance configured
- Ghost installed on EC2 instance
- Add Bulma
- Google Analytics
- AWS: EC2 Setup for Dev environment
- Created dev branch on Bitbucket

### Version 0.0.3 [Current]
- Configure webpack
- AWS: Create deployment file from Bitbucket repository
- AWS: Use S3 bucket to host theme files
- Setup staging subdomain
- Setup redirect maps for URL changes
- Create Public View of Trello board via JSON
- Set up email account on AWS.

### Version 0.0.4
- Write function to generate html comments reflecting each template name only in development mode.
- 

### Backlog

- Autopost to twitter after publshing post

###Commands

nodemon current/index.js --watch content/themes/mountain-man --ext hbs,js,css;